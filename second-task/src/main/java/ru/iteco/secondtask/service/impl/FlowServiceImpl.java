package ru.iteco.secondtask.service.impl;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import ru.iteco.itecocourse.error.ServiceException;
import ru.iteco.secondtask.model.ExternalInfo;
import ru.iteco.secondtask.service.ExternalService;
import ru.iteco.secondtask.service.FlowService;
import ru.iteco.secondtask.service.ProcessService;

import static ru.iteco.itecocourse.error.enumirate.ErrorStatusType.DATA_NULL_ERROR;

@Service
public class FlowServiceImpl implements FlowService {
    private final ExternalService externalService;
    private final ProcessService processService;

    public FlowServiceImpl(ExternalService externalService, @Lazy ProcessService processService) {
        this.externalService = externalService;
        this.processService = processService;
    }

    @Override
    public void run(Integer id) {

        ExternalInfo externalInfo = externalService.getExternalInfo(id);
        String info = externalInfo.getInfo();
        if (info != null) {
            processService.run(externalInfo);
        } else throw new ServiceException(DATA_NULL_ERROR, id);
    }
}
