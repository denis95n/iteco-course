package ru.iteco.secondtask.service;

import ru.iteco.secondtask.model.ExternalInfo;

public interface ProcessService {

    boolean run(ExternalInfo externalInfo);
}
