package ru.iteco.secondtask.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import ru.iteco.itecocourse.error.ServiceException;
import ru.iteco.secondtask.annotation.CheckRequest;
import ru.iteco.secondtask.model.ExternalInfo;
import ru.iteco.secondtask.service.ProcessService;

import static ru.iteco.itecocourse.error.enumirate.ErrorStatusType.NOT_PROCESS_ERROR;

@Service
@Lazy
public class ExternalInfoProcessServiceIml implements ProcessService {

    @Value("${id-not-process}")
    private int idNotProcess;

    @Override
    @CheckRequest
    public boolean run(ExternalInfo externalInfo) {
        if (idNotProcess == externalInfo.getId()) {
            throw new ServiceException(NOT_PROCESS_ERROR, idNotProcess);
        }

        return true;
    }
}
