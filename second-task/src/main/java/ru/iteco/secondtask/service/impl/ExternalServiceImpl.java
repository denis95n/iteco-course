package ru.iteco.secondtask.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.iteco.itecocourse.error.ServiceException;
import ru.iteco.secondtask.annotation.CacheResult;
import ru.iteco.secondtask.model.ExternalInfo;
import ru.iteco.secondtask.service.ExternalService;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.HashMap;

import static ru.iteco.itecocourse.error.enumirate.ErrorStatusType.DATA_BY_ID_NOT_FOUND_ERROR;


@Slf4j
@Service
public class ExternalServiceImpl implements ExternalService {

    private final HashMap<Integer, ExternalInfo> externalInfoHashMap = new HashMap<>();

    @PostConstruct
    private void init() {

        externalInfoHashMap.put(1, new ExternalInfo(1, "cs"));
        externalInfoHashMap.put(2, new ExternalInfo(2, "hasInfo"));
        externalInfoHashMap.put(3, new ExternalInfo(3, "info"));
        externalInfoHashMap.put(4, new ExternalInfo(4, "information"));
    }

    @PreDestroy
    private void destroy() {

        externalInfoHashMap.clear();
        log.info("HashMap очищен!");
    }

    @Override
    @CacheResult()
    public ExternalInfo getExternalInfo(Integer id) {

        if (!externalInfoHashMap.containsKey(id)) {
            throw new ServiceException(DATA_BY_ID_NOT_FOUND_ERROR, id);
        }

        return externalInfoHashMap.get(id);
    }
}
