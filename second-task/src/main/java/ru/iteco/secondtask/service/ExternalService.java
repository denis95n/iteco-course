package ru.iteco.secondtask.service;


import ru.iteco.secondtask.annotation.CacheResult;
import ru.iteco.secondtask.model.ExternalInfo;

public interface ExternalService {

    @CacheResult
    ExternalInfo getExternalInfo(Integer id);
}
