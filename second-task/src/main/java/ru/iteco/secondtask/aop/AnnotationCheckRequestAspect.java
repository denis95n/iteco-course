package ru.iteco.secondtask.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ru.iteco.itecocourse.error.ServiceException;
import ru.iteco.secondtask.model.ExternalInfo;

import java.util.Arrays;

import static ru.iteco.itecocourse.error.enumirate.ErrorStatusType.NOT_PROCESS_ERROR;


@Aspect
@Component
@Order(1)
@Slf4j
public class AnnotationCheckRequestAspect {
    @Value("${id-not-process}")
    private int idNotProcess;

    @Around("annotationCheckRequest()")
    public Object aroundCheckRequestAdvice(ProceedingJoinPoint joinPoint) throws Throwable {

        log.info("[Annotation(CheckRequest)] вызов {}", joinPoint.getSignature().toShortString());

        Arrays.stream(joinPoint.getArgs()).filter(o -> o.getClass().getSimpleName().equals("ExternalInfo"))
                .forEach(externalInfo -> {
                    if (((ExternalInfo) externalInfo).getId() == idNotProcess) {
                        throw new ServiceException(NOT_PROCESS_ERROR, idNotProcess);
                    }
                });

        return joinPoint.proceed();
    }

    @Pointcut("@annotation(ru.iteco.secondtask.annotation.CheckRequest) && " +
            "execution(* *(.., ru.iteco.secondtask.model.ExternalInfo,..))")
    public void annotationCheckRequest(){}
}