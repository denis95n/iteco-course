package ru.iteco.secondtask.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
public class LoggingAspect {

    @Before("allServiceAdvice()")
    public void beforeCallServiceAdvice(JoinPoint joinPoint) {

        log.info("[Before]{}: вызов метода {}({})",
                joinPoint.getSignature().getDeclaringType().getSimpleName(),
                joinPoint.getSignature().getName(),
                joinPoint.getArgs());
    }

    @After(value = "allServiceAdvice()")
    public void afterCallServiceAdvice(JoinPoint joinPoint) {

        log.info("[After]{}: получение данных {}({})",
                joinPoint.getSignature().getDeclaringType().getSimpleName(),
                joinPoint.getSignature().getName(),
                joinPoint.getArgs());
    }

    @Pointcut("within(ru.iteco.secondtask.service..*)")
    public void allServiceAdvice() {}
}
