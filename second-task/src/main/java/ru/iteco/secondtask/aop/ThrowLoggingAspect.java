package ru.iteco.secondtask.aop;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
public class ThrowLoggingAspect {

    @AfterThrowing(value = "allMethod()", throwing = "e")
    public void afterThrowLoggingAdvice(JoinPoint joinPoint, Throwable e) {
        log.error("[Exception] {}: {}", joinPoint.getSignature().getDeclaringType().getSimpleName(), e.getMessage());
    }

    @Pointcut("within(ru.iteco.secondtask..*)")
    public void allMethod () {}
}
