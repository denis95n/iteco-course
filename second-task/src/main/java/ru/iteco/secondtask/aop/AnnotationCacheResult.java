package ru.iteco.secondtask.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ru.iteco.secondtask.cache.CacheImpl;


@Aspect
@Component
@Order(0)
public class AnnotationCacheResult {

    @Autowired
    CacheImpl cache;

    @Around("annotationCacheResult()")
    public Object aroundAnnotationCacheResultAdvice(ProceedingJoinPoint joinPoint) throws Throwable {

        return cache.getCache(joinPoint);
    }

    @Pointcut("@annotation(ru.iteco.secondtask.annotation.CacheResult)")
    public void annotationCacheResult(){}
}
