package ru.iteco.secondtask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import ru.iteco.secondtask.service.impl.FlowServiceImpl;


@SpringBootApplication
@EnableAspectJAutoProxy
public class SecondTaskApplication {

    public static void main(String[] args) {

        ConfigurableApplicationContext run = SpringApplication.run(SecondTaskApplication.class, args);
        FlowServiceImpl flowServiceImpl = run.getBean(FlowServiceImpl.class);
        flowServiceImpl.run(1);
        flowServiceImpl.run(2);
        flowServiceImpl.run(3);
        flowServiceImpl.run(4);
    }
}
