package ru.iteco.secondtask.cache;

import org.aspectj.lang.ProceedingJoinPoint;

public interface Cache {
    Object getCache(ProceedingJoinPoint joinPoint) throws Throwable;
}
