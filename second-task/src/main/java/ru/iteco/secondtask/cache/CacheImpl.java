package ru.iteco.secondtask.cache;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Data
@AllArgsConstructor
@Slf4j
@Component
public class CacheImpl implements Cache{

    private final Map<String, Map<Object[], Object>> cache = new ConcurrentHashMap<>();

    @Override
    public Object getCache (ProceedingJoinPoint joinPoint) throws Throwable {

        String methodName = joinPoint.getSignature().getName();
        Object[] arguments = joinPoint.getArgs();
        Object data = null;

        if (!cache.containsKey(methodName)){
            Map<Object[], Object> newData = new ConcurrentHashMap<>();
            data = joinPoint.proceed();
            newData.put(arguments, data);
            cache.put(methodName, newData);
            log.info("[CacheResult] Вызов метода {}. Данные {}, добавлены в кеш.", methodName, data);
        } else {
            for(Map.Entry<String, Map<Object[], Object>> item : cache.entrySet()){
                if (item.getKey().equals(methodName)){
                    Map<Object[], Object> value = item.getValue();
                    for(Map.Entry<Object[], Object> itemValue : value.entrySet()){
                        Object[] itemValueKey = itemValue.getKey();
                        for(Object cacheArgument: itemValueKey){
                            for (Object argument: arguments){
                                if (cacheArgument == argument){
                                    log.info("[CacheResult] Метод {} есть в кеше. Возвращаем данные из кеша.", methodName);
                                    return itemValue.getValue();
                                }
                            }
                        }
                    }
                    data = joinPoint.proceed();
                    cache.put(methodName, Map.of(arguments, data));
                    log.info("[CacheResult] Вызов метода " + methodName + ". Данные " + data + ", добавлены в кеш.");
                }
            }
        }
        return data;
    }
}
