package ru.ireco.account.gateway.model;

import lombok.Data;

import java.util.List;

@Data
public class TickerDataRequest {

    private List<String> data;
}
