package ru.ireco.account.gateway.model;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Builder
@Data
public class TickerHistoricalDataRequest {

    private LocalDateTime date;
    private List<String> tickerNames;
}
