Программа курса:

▪️ Spring IoC и Spring DI. Инверсия контроля и внедрение зависимости;
▪️ Spring AOP. Аспекто-ориентированное программирование;
▪️ Spring MVC. Архитектура Model-View-Controller;
▪️ Component и Service;
▪️ Spring Data. JPA. Hibernate. Доступ к данным (БД);
▪️ Spring REST API;
▪️ Подключение REST Client;
▪️ Spring Security. Аутентификация и авторизация;
▪️ Spring Cloud. Распределенные системы.