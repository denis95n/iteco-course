package ru.iteco.itecocourse.error;

import ru.iteco.itecocourse.error.enumirate.ErrorStatusType;

public class ServiceException extends RuntimeException{

    public ServiceException(ErrorStatusType error){
        this(error.getDescription());
    }

    public ServiceException(ErrorStatusType error, Object... descParam){
        this(String.format(error.getDescription(), descParam));
    }

    public ServiceException(String error){
        super(error);
    }
}
