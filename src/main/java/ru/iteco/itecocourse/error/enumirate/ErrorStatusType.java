package ru.iteco.itecocourse.error.enumirate;

public enum ErrorStatusType {

    NOT_PROCESS_ERROR("%s, не является процессом!"),
    DATA_BY_ID_NOT_FOUND_ERROR("Значение по ключу %s, не найдено!"),
    DATA_NULL_ERROR("ID[%s] содержит пустую информацию!");

    private final String description;

    ErrorStatusType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
