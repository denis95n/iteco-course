package ru.iteco.account.currency.error;

public class ConvertException extends RuntimeException{

    public ConvertException(String message) {
        super(message);
    }
}