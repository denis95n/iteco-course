package ru.iteco.account.currency.hundler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.iteco.account.currency.error.ConvertException;
import ru.iteco.account.currency.error.ParamException;
import ru.iteco.account.currency.error.dto.ErrorDto;
import ru.iteco.account.currency.error.enumirate.ErrorStatusType;

@RestControllerAdvice
public class CurrencyExceptionHandler {

    @ExceptionHandler(ConvertException.class)
    public ResponseEntity<ErrorDto> handleConvertException(ConvertException exception) {

        ErrorDto errorDto = new ErrorDto(ErrorStatusType.CONVERT_ERROR, exception.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorDto);
    }

    @ExceptionHandler(ParamException.class)
    public ResponseEntity<ErrorDto> handleParamException(ParamException exception) {

        ErrorDto errorDto = new ErrorDto(ErrorStatusType.PARAM_ERROR, exception.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorDto);
    }
}
