package ru.iteco.account.currency.error.enumirate;

public enum ErrorStatusType {

    PARAM_ERROR("Ошибка параметров!"),
    CONVERT_ERROR("Ошибка конвертации!");

    private final String status;

    ErrorStatusType(String status) {
        this.status = status;
    }


    public String getStatus() {
        return status;
    }
}
