package ru.iteco.account.currency.error;

public class ParamException extends RuntimeException {

    public ParamException(String message) {
        super(message);
    }
}
