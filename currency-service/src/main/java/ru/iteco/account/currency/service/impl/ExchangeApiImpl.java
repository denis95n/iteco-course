package ru.iteco.account.currency.service.impl;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.iteco.account.currency.error.ConvertException;
import ru.iteco.account.currency.model.AllCurrencyExchange;
import ru.iteco.account.currency.model.ConvertResult;
import ru.iteco.account.currency.model.ConverterRequest;
import ru.iteco.account.currency.model.dto.ExchangeDto;
import ru.iteco.account.currency.service.ExchangeApi;
import ru.iteco.account.currency.service.ExchangeService;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
class ExchangeApiImpl implements ExchangeApi {

    private final RestTemplate restTemplate;
    private final String token;
    private final String urlAllExchange;
    private final ExchangeService exchangeService;
    private final String RUB = "RUB";

    public ExchangeApiImpl(@Qualifier("restTemplateExchange") RestTemplate restTemplate,
                           @Value("${currency.token}") String token,
                           @Value("${currency.url.all-exchange}") String urlAllExchange,
                           ExchangeService exchangeService) {
        this.restTemplate = restTemplate;
        this.token = token;
        this.urlAllExchange = urlAllExchange;
        this.exchangeService = exchangeService;
    }

    @Override
    public ConvertResult convert(ConverterRequest request) {

        if (Objects.equals(request.getAmount(), new BigDecimal("0.0"))) {
            return ConvertResult.builder()
                    .result(new BigDecimal("0.0"))
                    .query(request)
                    .build();
        }

        List<ExchangeDto> exchangeFrom = getExchangeDtoByName(request.getFrom());
        List<ExchangeDto> exchangeTo = getExchangeDtoByName(request.getTo());

        if (exchangeFrom.isEmpty() || exchangeTo.isEmpty()) {

            AllCurrencyExchange allCurrencyExchange = getAllCurrencyExchange();
            exchangeFrom = saveExchange(allCurrencyExchange, request.getFrom());
            exchangeTo = saveExchange(allCurrencyExchange, request.getTo());
        }

        return ConvertResult.builder()
                .result(
                        exchangeFrom.get(0).getRate()
                                .multiply(request.getAmount())
                                .divide(exchangeTo.get(0).getRate(), RoundingMode.CEILING))
                .query(request)
                .build();
    }

    private List<ExchangeDto> getExchangeDtoByName(String name) {

        return exchangeService.getByName(name).stream()
                .filter(exchangeDto ->
                        exchangeDto.getDate().isAfter(LocalDateTime.now().toLocalDate().atStartOfDay())
                                && exchangeDto.getDate().isBefore(LocalDateTime.of(LocalDate.now(), LocalTime.MAX)))
                .collect(Collectors.toList());
    }

    private List<ExchangeDto> saveExchange(AllCurrencyExchange allCurrencyExchange, String name) {

        List<ExchangeDto> exchangeDtoList = new ArrayList<>();

        if (allCurrencyExchange != null && !allCurrencyExchange.getRates().isEmpty()) {

            String EUR_BY_RUB = allCurrencyExchange.getRates().entrySet().stream()
                    .filter(entry -> Objects.equals(RUB, entry.getKey()))
                    .map(Map.Entry::getValue)
                    .findFirst()
                    .orElseThrow(() -> new ConvertException("Не удалось получить данные по валюте: " + RUB));

            allCurrencyExchange.getRates().entrySet().stream()
                    .filter(entry -> Objects.equals(name, entry.getKey()))
                    .forEach(entry ->
                            exchangeDtoList.add(
                                    exchangeService.save(
                                            ExchangeDto.builder()
                                                    .name(entry.getKey())
                                                    .date(LocalDateTime.now())
                                                    .rate(
                                                            BigDecimal.valueOf(Double.parseDouble(EUR_BY_RUB)).divide(
                                                                    BigDecimal.valueOf(Double.parseDouble(entry.getValue())),
                                                                    RoundingMode.CEILING
                                                            )
                                                    )
                                                    .build())
                            )
                    );
        }

        if (exchangeDtoList.isEmpty()) {
            throw new ConvertException("Не удалось получить данные по валюте: " + name);
        }

        return exchangeDtoList;
    }

    @Override
    public AllCurrencyExchange getAllCurrencyExchange() {

        ResponseEntity<AllCurrencyExchange> responseEntity = restTemplate.getForEntity(
                String.format(urlAllExchange, token),
                AllCurrencyExchange.class);

        return responseEntity.getBody();
    }
}
