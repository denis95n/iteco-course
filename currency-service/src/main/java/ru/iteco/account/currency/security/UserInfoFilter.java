package ru.iteco.account.currency.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.filter.GenericFilterBean;
import ru.iteco.account.currency.model.UserInfo;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class UserInfoFilter extends GenericFilterBean {

    private final ObjectMapper objectMapper;

    public UserInfoFilter(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String userInfoStr = request.getHeader("user-info");

        if (userInfoStr == null) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        UserInfo userInfo = objectMapper.readValue(userInfoStr, UserInfo.class);
        log.info("User info: {}", userInfo);

        JwtAuthenticationToken jwtToken = ((JwtAuthenticationToken) SecurityContextHolder.getContext().getAuthentication());

        List<GrantedAuthority> authorities = userInfo.getRoles().stream()
                .map(role -> "ROLE_" + role)
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());

        authorities.addAll(jwtToken.getAuthorities());

        SecurityContextHolder.getContext().setAuthentication(new JwtAuthenticationToken(jwtToken.getToken(), authorities));

        filterChain.doFilter(servletRequest, servletResponse);
    }

}
