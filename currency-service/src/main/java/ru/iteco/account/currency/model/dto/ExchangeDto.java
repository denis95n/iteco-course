package ru.iteco.account.currency.model.dto;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@Builder
public class ExchangeDto {

    Integer id;
    String name;
    LocalDateTime date;
    BigDecimal rate;
}
