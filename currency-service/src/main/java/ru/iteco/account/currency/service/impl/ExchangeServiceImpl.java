package ru.iteco.account.currency.service.impl;

import org.springframework.stereotype.Service;
import ru.iteco.account.currency.mapper.ExchangeMapper;
import ru.iteco.account.currency.model.dto.ExchangeDto;
import ru.iteco.account.currency.model.entity.ExchangeEntity;
import ru.iteco.account.currency.repository.ExchangeRepository;
import ru.iteco.account.currency.service.ExchangeService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ExchangeServiceImpl implements ExchangeService {

    private final ExchangeRepository exchangeRepository;

    public ExchangeServiceImpl(ExchangeRepository exchangeRepository) {
        this.exchangeRepository = exchangeRepository;
    }

    @Override
    public ExchangeDto save(ExchangeDto exchangeDto) {

        List<ExchangeDto> byName = getByName(exchangeDto.getName());

        if (!byName.isEmpty()){

            ExchangeDto exchangeDtoUpdate = byName.stream()
                    .findFirst().get();

            exchangeDto.setId(exchangeDtoUpdate.getId());
        }

        ExchangeEntity exchangeEntity = ExchangeMapper.INSTANCE.currencyDtoToEntity(exchangeDto);
        exchangeRepository.save(exchangeEntity);

        return ExchangeMapper.INSTANCE.exchangeEntityToDto(exchangeEntity);
    }

    @Override
    public List<ExchangeDto> getAll() {

        return exchangeRepository.findAll().stream()
                .map(ExchangeMapper.INSTANCE::exchangeEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<ExchangeDto> getByName(String name) {

        Optional<List<ExchangeEntity>> exchangeEntities = exchangeRepository.getByName(name);

        if (exchangeEntities.isEmpty()) {
            return new ArrayList<>();
        } else {
            return exchangeEntities.get().stream()
                    .map(ExchangeMapper.INSTANCE::exchangeEntityToDto)
                    .collect(Collectors.toList());
        }
    }
}
