package ru.iteco.account.currency.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import ru.iteco.account.currency.model.dto.ExchangeDto;
import ru.iteco.account.currency.model.entity.ExchangeEntity;

@Mapper
public interface ExchangeMapper {

    ExchangeMapper INSTANCE = Mappers.getMapper(ExchangeMapper.class);

    ExchangeDto exchangeEntityToDto(ExchangeEntity exchangeEntity);

    ExchangeEntity currencyDtoToEntity(ExchangeDto exchangeDto);
}
