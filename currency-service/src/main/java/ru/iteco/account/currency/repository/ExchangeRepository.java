package ru.iteco.account.currency.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.iteco.account.currency.model.entity.ExchangeEntity;

import java.util.List;
import java.util.Optional;

public interface ExchangeRepository extends JpaRepository<ExchangeEntity, Integer> {

    Optional<List<ExchangeEntity>> getByName(String name);
}
