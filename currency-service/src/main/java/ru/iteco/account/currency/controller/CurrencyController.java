package ru.iteco.account.currency.controller;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import ru.iteco.account.currency.error.ParamException;
import ru.iteco.account.currency.model.AllCurrencyExchange;
import ru.iteco.account.currency.model.ConvertResult;
import ru.iteco.account.currency.model.ConverterRequest;
import ru.iteco.account.currency.service.ExchangeApi;

@RestController
@RequiredArgsConstructor
@Slf4j
public class CurrencyController {

    private final ExchangeApi exchangeApi;

    @PostMapping(value = "/convert", produces = "application/json", consumes = "application/x-www-form-urlencoded")
    public ConvertResult convertAmount(@RequestBody String convert,
                                       @RequestHeader Map<String, String> headers) {

        log.info("headers: {}", headers);
        log.info("body: {}", convert);

        List<String> data = List.of(convert.split("&"));

        ConverterRequest converterRequest;

        try {
            converterRequest =
                    ConverterRequest.builder()
                            .from(data.get(0).substring(5))
                            .to(data.get(1).substring(3))
                            .amount(BigDecimal.valueOf(Double.parseDouble(data.get(2).substring(7))))
                            .build();
        } catch (Exception ex) {
            throw new ParamException("Заданы неверные параметры: " + convert);
        }

        return exchangeApi.convert(converterRequest);
    }

    @GetMapping("/all-exchange")
    public AllCurrencyExchange getAllExchange(@RequestHeader Map<String, String> headers) {
        log.info("Request with headers: {}", headers);
        return exchangeApi.getAllCurrencyExchange();
    }

}
