package ru.iteco.account.currency.service;

import ru.iteco.account.currency.model.dto.ExchangeDto;

import java.util.List;

public interface ExchangeService {

    ExchangeDto save(ExchangeDto exchangeDto);

    List<ExchangeDto> getAll ();

    List<ExchangeDto> getByName(String name);
}
