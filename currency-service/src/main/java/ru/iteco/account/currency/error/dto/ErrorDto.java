package ru.iteco.account.currency.error.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import ru.iteco.account.currency.error.enumirate.ErrorStatusType;

@Data
@Builder
@AllArgsConstructor
public class ErrorDto {

    private ErrorStatusType status;
    private String message;
}
