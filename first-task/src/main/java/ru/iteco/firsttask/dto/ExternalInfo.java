package ru.iteco.firsttask.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@AllArgsConstructor
@Component
public class ExternalInfo {
    Integer id;
    String info;

    ExternalInfo(){}
}
