package ru.iteco.firsttask.config.postprocessor;

import lombok.extern.java.Log;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.stereotype.Component;
import ru.iteco.firsttask.annotation.CacheResult;

import java.lang.reflect.Method;

@Component
@Log
public class FilterAnnotationPrototypeAndCacheResultBeanFactoryPostProcessor implements BeanFactoryPostProcessor {

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {
        String[] beanDefinitionNames = configurableListableBeanFactory.getBeanDefinitionNames();
        for(String beanDefinitionName : beanDefinitionNames) {
            BeanDefinition beanDefinition = configurableListableBeanFactory.getBeanDefinition(beanDefinitionName);
            String beanClassName = beanDefinition.getBeanClassName();
            if (beanClassName != null) {
                try {
                    Class<?> aClass = Class.forName(beanClassName);
                    Method[] declaredMethods = aClass.getDeclaredMethods();
                    for (Method method : declaredMethods) {
                        if (beanDefinition.isPrototype()) {
                            log.warning("Метод " + method.getName() + " имеет Scope prototype");
                        }

                        if (method.isAnnotationPresent(CacheResult.class)) {
                            log.warning("Метод " + method.getName() + " имеет аннотацию CacheResult");
                        }
                    }
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}