package ru.iteco.firsttask.config.postprocessor;

import lombok.extern.java.Log;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.aop.framework.ReflectiveMethodInvocation;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;
import org.yaml.snakeyaml.util.ArrayUtils;
import ru.iteco.firsttask.annotation.CacheResult;
import ru.iteco.firsttask.cache.Cache;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Component
@Log
public class CacheResultBeanPostProcessor implements BeanPostProcessor {

    @Autowired
    public Cache cache;

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        Method[] methods = bean.getClass().getMethods();
        for(Method method : methods) {
            final String methodName = method.getName();
            if (method.isAnnotationPresent(CacheResult.class)) {
                ProxyFactory proxyFactory = new ProxyFactory(bean);
                proxyFactory.addAdvice(new MethodInterceptor() {
                    @Override
                    public Object invoke(MethodInvocation invocation) throws Throwable {
                        Object proceed = invocation.proceed();
                        Method invocationMethod = invocation.getMethod();
                        if (invocationMethod.isAnnotationPresent(CacheResult.class)) {
                            log.info("Кеширование...");
                            Object[] arguments = invocation.getArguments();
                            return cache.getCache(methodName, arguments, proceed);
                        }
                        return proceed;
                    }
                });
                return proxyFactory.getProxy();
            }
        }
        return bean;
    }
}
