package ru.iteco.firsttask.cache;

public interface Cache {
    Object getCache(String methodName, Object[] arguments, Object data);
}
