package ru.iteco.firsttask.cache;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.java.Log;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Data
@AllArgsConstructor
@Log
@Component
public class CacheImpl implements Cache{

    private final Map<String, Map<Object[], Object>> cache = new HashMap<>();

    @Override
    public Object getCache (String methodName, Object[] arguments, Object data){
        if (!cache.containsKey(methodName)){
            Map<Object[], Object> newData = new HashMap<>();
            newData.put(arguments, data);
            cache.put(methodName, newData);
            log.info("Вызов метода " + methodName + ". Данные " + data + ", добавлены в кеш.");
        } else {
            for(Map.Entry<String, Map<Object[], Object>> item : cache.entrySet()){
                if (item.getKey().equals(methodName)){
                    Map<Object[], Object> value = item.getValue();
                    for(Map.Entry<Object[], Object> itemValue : value.entrySet()){
                        Object[] itemValueKey = itemValue.getKey();
                        for(Object cacheArgument: itemValueKey){
                            for (Object argument: arguments){
                                if (cacheArgument == argument){
                                    log.info("Метод " + methodName + " в кеше.");
                                    return itemValue.getValue();
                                }
                            }
                        }
                    }
                    cache.put(methodName, Map.of(arguments, data));
                    log.info("Вызов метода " + methodName + ". Данные " + data + ", добавлены в кеш.");
                }
            }
        }
        return data;
    }
}
