package ru.iteco.firsttask.service.impl;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import ru.iteco.firsttask.dto.ExternalInfo;
import ru.iteco.firsttask.service.ProcessService;

@Service
@Log
@Lazy
public class ExternalInfoProcessIml implements ProcessService {

    @Value("${id-not-process}")
    private int idNotProcess;

    @Override
    public boolean run(ExternalInfo externalInfo) {
        log.info("Проверка процесса...");
        if (idNotProcess == externalInfo.getId()){
            log.info(idNotProcess + ", не является процессом!");
            return false;
        }
        log.info("Успешно! Запуск процесса: " + externalInfo.getId());
        return true;
    }
}
