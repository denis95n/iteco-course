package ru.iteco.firsttask.service;

import ru.iteco.firsttask.annotation.CacheResult;
import ru.iteco.firsttask.dto.ExternalInfo;


public interface ExternalService {

    @CacheResult
    ExternalInfo getExternalInfo(Integer id);
}
