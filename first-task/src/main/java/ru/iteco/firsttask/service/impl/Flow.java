package ru.iteco.firsttask.service.impl;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import ru.iteco.firsttask.dto.ExternalInfo;
import ru.iteco.firsttask.service.ExternalService;
import ru.iteco.firsttask.service.FlowService;
import ru.iteco.firsttask.service.ProcessService;

@Service
@Log
public class Flow implements FlowService {
    private final ExternalService externalService;
    private final ProcessService processService;

    public Flow(ExternalService externalService, @Lazy ProcessService processService) {
        this.externalService = externalService;
        this.processService = processService;
    }

    @Override
    public void run(Integer id) {
        ExternalInfo externalInfo = externalService.getExternalInfo(id);
        String info = externalInfo.getInfo();
        if (info != null) {
            processService.run(externalInfo);
            log.info("Получена следующая информация: " + info);
        } else log.info("ID[" + externalInfo.getId() + "] содержит пустую информацию!");
    }
}
