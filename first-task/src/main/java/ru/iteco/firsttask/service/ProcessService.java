package ru.iteco.firsttask.service;

import ru.iteco.firsttask.dto.ExternalInfo;

public interface ProcessService {

    boolean run(ExternalInfo externalInfo);
}
