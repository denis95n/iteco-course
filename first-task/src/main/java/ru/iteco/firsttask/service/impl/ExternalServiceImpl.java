package ru.iteco.firsttask.service.impl;

import lombok.extern.java.Log;
import org.springframework.stereotype.Service;
import ru.iteco.firsttask.annotation.CacheResult;
import ru.iteco.firsttask.dto.ExternalInfo;
import ru.iteco.firsttask.service.ExternalService;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.HashMap;


@Log
@Service
public class ExternalServiceImpl implements ExternalService {

    private final HashMap<Integer, ExternalInfo> externalInfoHashMap = new HashMap<>();

    @PostConstruct
    private void init() {

        externalInfoHashMap.put(1, new ExternalInfo(1, null));
        externalInfoHashMap.put(2, new ExternalInfo(2, "hasInfo"));
        externalInfoHashMap.put(3, new ExternalInfo(3, "info"));
        externalInfoHashMap.put(4, new ExternalInfo(4, "information"));
    }

    @PreDestroy
    private void destroy() {

        externalInfoHashMap.clear();
        log.info("HashMap очищен!");
    }

    @Override
    @CacheResult()
    public ExternalInfo getExternalInfo(Integer id) {
        log.info("Получение внешний информации с ID[" + id + "]");
        return externalInfoHashMap.get(id);
    }
}
