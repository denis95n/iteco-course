package ru.iteco.stockservice.model;

import lombok.Data;

import java.util.List;

@Data
public class TickerDataResponse {

    private List<Info> data;
}
