package ru.iteco.stockservice.model;

import lombok.Data;

import java.util.List;

@Data
public class TickerHistoricalResponse {

    private List<HistoricalInfo> data;
}
