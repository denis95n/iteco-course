package ru.iteco.stockservice.model;

import lombok.Data;

import java.util.List;

@Data
public class TickerHistoricalDataRequest {

    private List<String> tickets;
    private String date;
}
