package ru.iteco.stockservice.service;

import ru.iteco.stockservice.model.TickerDataResponse;
import ru.iteco.stockservice.model.TickerHistoricalDataRequest;
import ru.iteco.stockservice.model.TickerHistoricalResponse;

import java.util.List;

public interface StockApi {

    TickerDataResponse getTickerData(List<String> tickets);

    TickerHistoricalResponse getTickerHistoricalData(TickerHistoricalDataRequest request);

}
