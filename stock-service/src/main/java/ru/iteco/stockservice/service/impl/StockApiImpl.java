package ru.iteco.stockservice.service.impl;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.iteco.stockservice.model.TickerDataResponse;
import ru.iteco.stockservice.model.TickerHistoricalDataRequest;
import ru.iteco.stockservice.model.TickerHistoricalResponse;
import ru.iteco.stockservice.service.StockApi;

import java.util.List;

@Service
public class StockApiImpl implements StockApi {

    private final RestTemplate restTemplate;
    private final String token;
    private final String urlTickerData;
    private final String urlTickerHistoricalData;

    public StockApiImpl(@Qualifier("restTemplateExchange") RestTemplate restTemplate,
                        @Value("${stock.token}") String token,
                        @Value("${stock.url.ticker-data}") String urlTickerData,
                        @Value("${stock.url.historical-data}") String urlTickerHistoricalData) {
        this.restTemplate = restTemplate;
        this.token = token;
        this.urlTickerData = urlTickerData;
        this.urlTickerHistoricalData = urlTickerHistoricalData;
    }

    @Override
    public TickerDataResponse getTickerData(List<String> tickets) {

        String collectTicket = String.join(",", tickets);

        ResponseEntity<TickerDataResponse> forEntity = restTemplate.getForEntity(
                String.format(urlTickerData, collectTicket, token),
                TickerDataResponse.class
        );
        return forEntity.getBody();
    }

    @Override
    public TickerHistoricalResponse getTickerHistoricalData(TickerHistoricalDataRequest request) {

        String collectTicket = String.join(",", request.getTickets());

        ResponseEntity<TickerHistoricalResponse> forEntity = restTemplate.getForEntity(
                String.format(
                        urlTickerHistoricalData,
                        collectTicket,
                        request.getDate(),
                        token),
                TickerHistoricalResponse.class
        );
        return forEntity.getBody();
    }
}
