package ru.iteco.stockservice.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import ru.iteco.stockservice.model.TickerDataResponse;
import ru.iteco.stockservice.model.TickerHistoricalDataRequest;
import ru.iteco.stockservice.model.TickerHistoricalResponse;
import ru.iteco.stockservice.service.StockApi;

import java.util.List;
import java.util.Map;

@RestController
@Slf4j
public class StockController {

    private final StockApi stockApi;

    public StockController(StockApi stockApi) {
        this.stockApi = stockApi;
    }

    @PostMapping(value = "/ticker-data", produces = "application/json", consumes = "application/x-www-form-urlencoded")
    public TickerDataResponse getTickerData(@RequestBody String tickets,
                                            @RequestHeader Map<String, String> headers) {

        log.info("headers: {}", headers);
        log.info("body: {}", tickets);
        tickets = tickets.substring(5);
        List<String> ticket = List.of(tickets.split(","));
        return stockApi.getTickerData(ticket);
    }

    @PostMapping(value = "/historical-data", produces = "application/json", consumes = "application/x-www-form-urlencoded")
    public TickerHistoricalResponse getTickerHistoricalData(@RequestBody String historical,
                                                            @RequestHeader Map<String, String> headers) {

        log.info("headers: {}", headers);
        log.info("body: {}", historical);

        List<String> data = List.of(historical.split("&"));

        TickerHistoricalDataRequest request = new TickerHistoricalDataRequest();
        request.setTickets(List.of(data.get(0).substring(data.get(0).indexOf('=') + 1).split(",")));
        request.setDate(data.get(1).substring(data.get(1).indexOf('=') + 1));

        return stockApi.getTickerHistoricalData(request);
    }
}
