package ru.iteco.thirdtask.validator;

import ru.iteco.thirdtask.annotation.Currency;
import ru.iteco.thirdtask.mapper.CurrencyMapper;
import ru.iteco.thirdtask.model.dto.CurrencyDto;
import ru.iteco.thirdtask.model.entity.CurrencyEntity;
import ru.iteco.thirdtask.repository.CurrencyRepository;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;
import java.util.Set;

public class CurrencyValidator implements ConstraintValidator<Currency, String> {

    private final CurrencyRepository currencyRepository;
    private static final Set<String> CURRENCY = Set.of("RUB", "EUR", "USD", "GBP");

    public CurrencyValidator(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    @Override
    public boolean isValid(String currencyName, ConstraintValidatorContext constraintValidatorContext) {

        return !Objects.isNull(currencyRepository.getByName(currencyName));
    }
}
