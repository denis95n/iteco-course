package ru.iteco.thirdtask.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.iteco.thirdtask.model.entity.CurrencyEntity;

public interface CurrencyRepository extends JpaRepository<CurrencyEntity, Integer> {

    CurrencyEntity getByName(String name);
}
