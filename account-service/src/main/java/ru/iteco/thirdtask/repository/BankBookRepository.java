package ru.iteco.thirdtask.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.iteco.thirdtask.model.entity.BankBookEntity;

import java.util.List;
import java.util.Optional;

public interface BankBookRepository extends JpaRepository<BankBookEntity, Integer> {

    List<BankBookEntity> getByUserId(String userId);
    Optional<BankBookEntity> getByNumber(String number);
}
