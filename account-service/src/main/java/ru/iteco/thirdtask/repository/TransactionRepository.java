package ru.iteco.thirdtask.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.iteco.thirdtask.model.entity.TransactionEntity;

public interface TransactionRepository extends JpaRepository<TransactionEntity, Integer> {
}
