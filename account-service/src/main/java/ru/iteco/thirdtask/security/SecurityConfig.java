package ru.iteco.thirdtask.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.server.resource.web.BearerTokenAuthenticationFilter;

@Configuration
@AllArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final ObjectMapper objectMapper;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/transact").hasAuthority("ROLE_pro")
                .antMatchers("/info").hasAnyAuthority("ROLE_base", "ROLE_pro")
                .antMatchers("/actuator/**").anonymous()
                .and()
                .authorizeRequests().anyRequest().authenticated()
                .and()
                .oauth2ResourceServer().jwt();

        http.addFilterAfter(userInfoFilter(), BearerTokenAuthenticationFilter.class);
    }

    @Bean
    UserInfoFilter userInfoFilter() {
        return new UserInfoFilter(objectMapper);
    }
}
