package ru.iteco.thirdtask.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import ru.iteco.thirdtask.annotation.Currency;
import ru.iteco.thirdtask.validator.Created;
import ru.iteco.thirdtask.validator.Update;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;
import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@Builder
public class BankBookDto {

    @Null(groups = Created.class)
    @NotNull(groups = Update.class)
    Integer id;

    @NotNull
    String userId;

    @NotBlank
    String number; //номер счета

    @PositiveOrZero
    BigDecimal amount;

    BigDecimal amountRUB;

    @Currency
    CurrencyDto currency; //тип валюты
}
