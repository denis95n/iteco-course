package ru.iteco.thirdtask.model.dto;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class TransactionDto {

    private String sourceBankBookId;
    private String targetBankBookId;
    private BigDecimal amount;
}
