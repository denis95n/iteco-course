package ru.iteco.thirdtask.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import ru.iteco.thirdtask.validator.Created;
import ru.iteco.thirdtask.validator.Update;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Null;

@Data
@AllArgsConstructor
@Builder
public class CurrencyDto {

    @Null(groups = Created.class)
    @NotNull(groups = Update.class)
    Integer id;
    String name;
}
