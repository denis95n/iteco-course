package ru.iteco.thirdtask.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import ru.iteco.thirdtask.model.dto.BankBookDto;
import ru.iteco.thirdtask.model.entity.BankBookEntity;

@Mapper
public interface BankBookMapper {

    BankBookMapper INSTANCE = Mappers.getMapper(BankBookMapper.class);

    BankBookDto bankBookEntityToDto(BankBookEntity bankBook);

    BankBookEntity bankBookDtoToEntity(BankBookDto bankBook);
}
