package ru.iteco.thirdtask.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import ru.iteco.thirdtask.model.dto.CurrencyDto;
import ru.iteco.thirdtask.model.entity.CurrencyEntity;

@Mapper
public interface CurrencyMapper {

    CurrencyMapper currencyMapper = Mappers.getMapper(CurrencyMapper.class);

    CurrencyDto currencyEntityToDto(CurrencyEntity currency);

    CurrencyEntity currencyDtoToEntity(CurrencyDto currency);
}
