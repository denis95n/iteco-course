package ru.iteco.thirdtask.hundler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.iteco.thirdtask.error.exception.*;
import ru.iteco.thirdtask.error.dto.ErrorDto;

import javax.persistence.EntityNotFoundException;

import static ru.iteco.thirdtask.error.enumirate.ErrorStatusType.*;

@RestControllerAdvice
public class BankBookExceptionHandler {

    @ExceptionHandler(ParamValidateException.class)
    public ResponseEntity<ErrorDto> handleServiceExceptionException(ParamValidateException exception) {

        ErrorDto errorDto = new ErrorDto(PARAM_NOT_SET, exception.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorDto);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<ErrorDto> handleServiceExceptionException(EntityNotFoundException exception) {

        ErrorDto errorDto = new ErrorDto(DATA_NOT_FOUND, exception.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorDto);
    }

    @ExceptionHandler(RequestException.class)
    public ResponseEntity<ErrorDto> handleServiceExceptionException(RequestException exception) {

        ErrorDto errorDto = new ErrorDto(NUMBER_ERROR, exception.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorDto);
    }

    @ExceptionHandler(BankBookNotFoundException.class)
    public ResponseEntity<ErrorDto> handleServiceExceptionException(BankBookNotFoundException exception) {

        ErrorDto errorDto = new ErrorDto(NUMBER_ERROR, exception.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorDto);
    }

    @ExceptionHandler(CurrencyNotEqualsException.class)
    public ResponseEntity<ErrorDto> handleServiceExceptionException(CurrencyNotEqualsException exception) {

        ErrorDto errorDto = new ErrorDto(NUMBER_ERROR, exception.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorDto);
    }

    @ExceptionHandler(ParamException.class)
    public ResponseEntity<ErrorDto> handleServiceExceptionException(ParamException exception) {

        ErrorDto errorDto = new ErrorDto(NUMBER_ERROR, exception.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorDto);
    }

    @ExceptionHandler(AccessException.class)
    public ResponseEntity<ErrorDto> handleServiceExceptionException(AccessException exception) {

        ErrorDto errorDto = new ErrorDto(ACCESS_ERROR, exception.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorDto);
    }
}
