package ru.iteco.thirdtask.hundler;

import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.iteco.thirdtask.error.dto.ErrorDto;

import javax.validation.ConstraintViolationException;
import java.util.stream.Collectors;

import static ru.iteco.thirdtask.error.enumirate.ErrorStatusType.VALIDATION_ERROR;

@RestControllerAdvice
public class ValidationExceptionHandler {

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ConstraintViolationException.class)
    public ErrorDto handleServiceExceptionException(ConstraintViolationException exception) {
        return ErrorDto.builder().status(VALIDATION_ERROR).message(exception.getLocalizedMessage()).build();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ErrorDto handleMethodArgumentNotValidException(MethodArgumentNotValidException exception) {
        String errorMessage = exception.getBindingResult().getAllErrors().stream().map(objectError -> {
            String field = ((FieldError) objectError).getField();
            return field + ": " + objectError.getDefaultMessage();
        }).collect(Collectors.joining("; "));
        return ErrorDto.builder().status(VALIDATION_ERROR).message(errorMessage).build();
    }
}
