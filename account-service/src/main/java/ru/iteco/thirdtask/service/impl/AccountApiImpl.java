package ru.iteco.thirdtask.service.impl;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import ru.iteco.thirdtask.model.ConvertResult;
import ru.iteco.thirdtask.model.ConverterRequest;
import ru.iteco.thirdtask.service.AccountApi;

@Service
public class AccountApiImpl implements AccountApi {

    private final RestTemplate restTemplate;
    private final String urlConvert;

    public AccountApiImpl(@Qualifier("restTemplateExchange") RestTemplate restTemplate,
                          @Value("${currency.url.convert}") String urlConvert) {
        this.restTemplate = restTemplate;
        this.urlConvert = urlConvert;
    }

    @Override
    public ConvertResult convert(ConverterRequest converterRequest, HttpHeaders headers) {


//        SecurityContext context = SecurityContextHolder.getContext();
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
//        headers.add("authorization", "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjlFNXM2cjFHVkYybkM2bjR4REh4USJ9.eyJpc3MiOiJodHRwczovL2Rldi00Y20tbzlpYi51cy5hdXRoMC5jb20vIiwic3ViIjoic3ZOYTdmS0Z5eGpQZW5lU2lvYWh0aU14UkQyeWg0eXRAY2xpZW50cyIsImF1ZCI6ImFjY291bnQtc2VydmljZSIsImlhdCI6MTY0MzY0Mjc4NCwiZXhwIjoxNjQzNzI5MTg0LCJhenAiOiJzdk5hN2ZLRnl4alBlbmVTaW9haHRpTXhSRDJ5aDR5dCIsImd0eSI6ImNsaWVudC1jcmVkZW50aWFscyJ9.U7EbW1GplKcLTrMpaJqvA5lEJgQBJpwRPFZ69zDa612HT3kKw6wi-J9o9jW_bCj7ynGy6U4p17IiszVlMcvnGBqYqd4jsvTy8T-DjXnhEJ_kPTFEv55AUezezexLD19RYOpTPSBKnVggLXxMUASqGsdeezrl_TwOyz35qZwahoVyXwMOT8H3rtLFBGGKaFS6utpF-EHJx9uriiOX7DnFw81XDc7h2zaROW4hW-cdpritDpMfU6-_0WpePVmZQpdfmKRFKr247yJ5BMMJRqmOuuzKl1AoThPsxXdFPbr-vL5tXcIF9WObeTQBDViMpi2NQblcZo1nqU5uPzt_NVRe6g");

        MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
        map.add("from", converterRequest.getFrom());
        map.add("to", converterRequest.getTo());
        map.add("amount", String.valueOf(converterRequest.getAmount()));

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

        ResponseEntity<ConvertResult> responseEntity = restTemplate.postForEntity(
                urlConvert,
                request,
                ConvertResult.class);

        return responseEntity.getBody();
    }
}
