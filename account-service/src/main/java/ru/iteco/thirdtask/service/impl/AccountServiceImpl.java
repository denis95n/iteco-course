package ru.iteco.thirdtask.service.impl;

import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;
import ru.iteco.thirdtask.mapper.BankBookMapper;
import ru.iteco.thirdtask.model.ConverterRequest;
import ru.iteco.thirdtask.model.dto.BankBookDto;
import ru.iteco.thirdtask.repository.BankBookRepository;
import ru.iteco.thirdtask.service.AccountApi;
import ru.iteco.thirdtask.service.AccountService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AccountServiceImpl implements AccountService {

    private final BankBookRepository bankBookRepository;
    private final AccountApi accountApi;
    private final String RUB = "RUB";

    public AccountServiceImpl(BankBookRepository bankBookRepository, AccountApi accountApi) {
        this.bankBookRepository = bankBookRepository;
        this.accountApi = accountApi;
    }

    @Override
    public List<BankBookDto> getBankBookByUserId(String id, HttpHeaders headers) {

        return bankBookRepository.getByUserId(id)
                .stream()
                .map(BankBookMapper.INSTANCE::bankBookEntityToDto)
                .peek(bankBookDto -> {
                    if (!bankBookDto.getCurrency().getName().equals(RUB)) {
                        bankBookDto.setAmountRUB(
                                accountApi.convert(
                                                ConverterRequest.builder()
                                                        .from(bankBookDto.getCurrency().getName())
                                                        .to(RUB)
                                                        .amount(bankBookDto.getAmount())
                                                        .build(),
                                                headers)
                                        .getResult());
                    }
                })
                .collect(Collectors.toList());
    }
}
