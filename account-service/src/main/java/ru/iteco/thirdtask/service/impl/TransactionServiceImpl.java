package ru.iteco.thirdtask.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.iteco.thirdtask.error.exception.AccessException;
import ru.iteco.thirdtask.error.exception.BankBookNotFoundException;
import ru.iteco.thirdtask.error.exception.CurrencyNotEqualsException;
import ru.iteco.thirdtask.model.Status;
import ru.iteco.thirdtask.model.UserInfo;
import ru.iteco.thirdtask.model.dto.TransactionDto;
import ru.iteco.thirdtask.model.entity.BankBookEntity;
import ru.iteco.thirdtask.model.entity.TransactionEntity;
import ru.iteco.thirdtask.repository.BankBookRepository;
import ru.iteco.thirdtask.repository.StatusRepository;
import ru.iteco.thirdtask.repository.TransactionRepository;
import ru.iteco.thirdtask.service.TransactionService;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
@Slf4j
class TransactionServiceImpl implements TransactionService {

    private final BankBookRepository bankBookRepository;
    private final StatusRepository statusRepository;
    private final TransactionRepository transactionRepository;

    @Override
    @Transactional
    public Boolean transferBetweenBankBook(TransactionDto transactionDto, UserInfo userInfo) {


        BankBookEntity source = bankBookRepository.getByNumber(transactionDto.getSourceBankBookId()).orElseThrow(() -> new BankBookNotFoundException("Счет не найден"));
        BankBookEntity target = bankBookRepository.getByNumber(transactionDto.getTargetBankBookId()).orElseThrow(() -> new BankBookNotFoundException("Счет не найден"));

        if (!source.getUser().getId().equals(userInfo.getId())){
            throw new AccessException("Счет не пренадлежит данному пользователю!");
        }

        if (!source.getCurrency().equals(target.getCurrency())) {
            throw new CurrencyNotEqualsException("У счетов разные валюты!");
        }

        BigDecimal amount = transactionDto.getAmount();

        TransactionEntity transactionEntity = new TransactionEntity();
        transactionEntity.setSourceBankBook(source);
        transactionEntity.setTargetBankBook(target);
        transactionEntity.setAmount(amount);
        transactionEntity.setInitiationDate(LocalDateTime.now());
        transactionEntity.setStatus(statusRepository.findByName(Status.PROCESSING.getStatus()));

        if (source.getAmount().compareTo(amount) < 0) {
            transactionEntity.setStatus(statusRepository.findByName(Status.DECLINED.getStatus()));
            transactionEntity.setCompletionDate(LocalDateTime.now());
            transactionRepository.save(transactionEntity);
            return false;
        }

        source.setAmount(source.getAmount().subtract(amount));
        target.setAmount(target.getAmount().add(amount));

        bankBookRepository.save(source);
        bankBookRepository.save(target);

        transactionEntity.setStatus(statusRepository.findByName(Status.SUCCESSFUL.getStatus()));
        transactionEntity.setCompletionDate(LocalDateTime.now());
        transactionRepository.save(transactionEntity);
        return true;
    }
}
