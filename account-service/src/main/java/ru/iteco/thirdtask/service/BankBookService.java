package ru.iteco.thirdtask.service;

import ru.iteco.thirdtask.model.dto.BankBookDto;

import java.util.List;

public interface BankBookService {

    List<BankBookDto> getAll();

    List<BankBookDto> getAllByUserId(Integer id);

    BankBookDto getById(Integer id);

    BankBookDto create(BankBookDto bankBookDto);

    BankBookDto update(BankBookDto bankBookDto);

    void deleteById(Integer bankBookId);

    void deleteByUserId(Integer userId);
}
