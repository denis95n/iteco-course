package ru.iteco.thirdtask.service;

import org.springframework.http.HttpHeaders;
import ru.iteco.thirdtask.model.dto.BankBookDto;

import java.util.List;

public interface AccountService {

    List<BankBookDto> getBankBookByUserId (String id, HttpHeaders headers);
}
