package ru.iteco.thirdtask.service;

import ru.iteco.thirdtask.model.UserInfo;
import ru.iteco.thirdtask.model.dto.TransactionDto;

public interface TransactionService {

    Boolean transferBetweenBankBook(TransactionDto transactionDto, UserInfo userInfo);

}
