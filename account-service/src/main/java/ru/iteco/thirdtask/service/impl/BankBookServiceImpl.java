package ru.iteco.thirdtask.service.impl;

import org.springframework.stereotype.Service;
import ru.iteco.thirdtask.error.exception.RequestException;
import ru.iteco.thirdtask.mapper.BankBookMapper;
import ru.iteco.thirdtask.model.dto.BankBookDto;
import ru.iteco.thirdtask.model.entity.BankBookEntity;
import ru.iteco.thirdtask.repository.BankBookRepository;
import ru.iteco.thirdtask.service.BankBookService;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class BankBookServiceImpl implements BankBookService {

    private final BankBookRepository bankBookRepository;

    public BankBookServiceImpl(BankBookRepository bankBookRepository) {
        this.bankBookRepository = bankBookRepository;
    }

    @Override
    public List<BankBookDto> getAll() {

        return bankBookRepository.findAll().stream()
                .map(BankBookMapper.INSTANCE::bankBookEntityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<BankBookDto> getAllByUserId(Integer id) {

        return bankBookRepository.findAll().stream()
                .map(BankBookMapper.INSTANCE::bankBookEntityToDto)
                .filter(bankBook -> Objects.equals(bankBook.getUserId(), id))
                .collect(Collectors.toList());
    }

    @Override
    public BankBookDto getById(Integer id) {
        return BankBookMapper.INSTANCE.bankBookEntityToDto(bankBookRepository.getById(id));
    }

    @Override
    public BankBookDto create(BankBookDto newBankBookDto) {

        List<BankBookDto> bankBookDtoList = getAllByUserId(Integer.valueOf(newBankBookDto.getUserId()));

        bankBookDtoList.stream()
                .filter(bankBookDto -> bankBookDto.getNumber().equals(newBankBookDto.getNumber()))
                .forEach(bankBookDto -> {
                    if (bankBookDto.getCurrency().equals(newBankBookDto.getCurrency())) {
                        throw new RequestException("Счет с данной валютой уже имеется в хранилище: " + bankBookDto.getId());
                    }
                });

        BankBookEntity bankBookEntity = BankBookMapper.INSTANCE.bankBookDtoToEntity(newBankBookDto);
        bankBookRepository.save(bankBookEntity);

        return BankBookMapper.INSTANCE.bankBookEntityToDto(bankBookEntity);
    }

    @Override
    public BankBookDto update(BankBookDto newBankBook) {

        BankBookDto bankBookDto = getById(newBankBook.getId());

        if (!bankBookDto.getNumber().equals(newBankBook.getNumber())) {
            throw new RequestException("Номер менять нельзя!");
        }

        BankBookEntity bankBookEntity = BankBookMapper.INSTANCE.bankBookDtoToEntity(newBankBook);
        bankBookRepository.save(bankBookEntity);

        return BankBookMapper.INSTANCE.bankBookEntityToDto(bankBookEntity);
    }

    @Override
    public void deleteById(Integer bankBookId) {
        bankBookRepository.deleteById(bankBookId);
    }

    @Override
    public void deleteByUserId(Integer userId) {

        bankBookRepository.findAll().stream()
                .map(BankBookMapper.INSTANCE::bankBookEntityToDto)
                .filter(bankBookDto -> Objects.equals(bankBookDto.getUserId(), userId))
                .forEach(bankBookDto -> bankBookRepository.deleteById(bankBookDto.getId()));
    }
}
