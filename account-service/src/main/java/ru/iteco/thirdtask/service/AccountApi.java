package ru.iteco.thirdtask.service;

import org.springframework.http.HttpHeaders;
import ru.iteco.thirdtask.model.ConvertResult;
import ru.iteco.thirdtask.model.ConverterRequest;

public interface AccountApi {

    ConvertResult convert (ConverterRequest converterRequest, HttpHeaders headers);
}
