package ru.iteco.thirdtask.error.exception;

public class RequestException extends RuntimeException{

    public RequestException(String message) {
        super(message);
    }
}
