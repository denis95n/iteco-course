package ru.iteco.thirdtask.error.exception;

public class ParamValidateException extends RuntimeException {

    public ParamValidateException(String message) {
        super(message);
    }
}
