package ru.iteco.thirdtask.error.exception;

public class ParamException extends RuntimeException {

    public ParamException(String message) {
        super(message);
    }
}
