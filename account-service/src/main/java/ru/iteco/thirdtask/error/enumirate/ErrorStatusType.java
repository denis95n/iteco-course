package ru.iteco.thirdtask.error.enumirate;

public enum ErrorStatusType {

    DATA_NOT_FOUND("Данные не найдены!"),
    ACCESS_ERROR("Ошибка доступа!"),
    PARAM_NOT_SET("Параметры не заданы!"),
    PARAM_ERROR("Ошибка параметров!"),
    NUMBER_ERROR("Ошибка работы со счетом!"),
    VALIDATION_ERROR("Ошибка валидации!");

    private final String status;

    ErrorStatusType(String status) {
        this.status = status;
    }


    public String getStatus() {
        return status;
    }
}
