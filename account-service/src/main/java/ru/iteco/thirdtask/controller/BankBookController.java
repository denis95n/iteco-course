package ru.iteco.thirdtask.controller;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.iteco.thirdtask.model.dto.BankBookDto;
import ru.iteco.thirdtask.service.BankBookService;
import ru.iteco.thirdtask.validator.Created;
import ru.iteco.thirdtask.validator.Update;

import javax.validation.Valid;
import java.util.List;

@RestController
@Validated
@RequestMapping("/bank-book")
public class BankBookController {

    private final BankBookService bankBookService;

    public BankBookController(BankBookService bankBookService) {
        this.bankBookService = bankBookService;
    }

    @GetMapping("/by-user-id/{userId}")
    public List<BankBookDto> getAllBankBoolByUserId(@PathVariable Integer userId) {

        return bankBookService.getAllByUserId(userId);
    }

    @GetMapping("/{bankBookId}")
    public BankBookDto getBankBookById(@PathVariable Integer bankBookId){

        return bankBookService.getById(bankBookId);
    }

    @Validated(Created.class)
    @PostMapping()
    public BankBookDto createBankBook(@Valid @RequestBody BankBookDto bankBookDto){

        return bankBookService.create(bankBookDto);
    }

    @Validated(Update.class)
    @PutMapping()
    public BankBookDto updateBankBook(@Valid @RequestBody BankBookDto bankBookDto){

        return bankBookService.update(bankBookDto);
    }

    @DeleteMapping("/{bankBookId}")
    public void deleteBankBookById(@PathVariable Integer bankBookId){

        bankBookService.deleteById(bankBookId);
    }

    @DeleteMapping("/by-user-id/{userId}")
    public void deleteBankBookByUserId(@PathVariable Integer userId){

        bankBookService.deleteByUserId(userId);
    }
}
