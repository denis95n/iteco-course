package ru.iteco.thirdtask.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.iteco.thirdtask.error.exception.ParamException;
import ru.iteco.thirdtask.error.exception.ValidateException;
import ru.iteco.thirdtask.model.UserInfo;
import ru.iteco.thirdtask.model.dto.BankBookDto;
import ru.iteco.thirdtask.model.dto.TransactionDto;
import ru.iteco.thirdtask.service.AccountService;
import ru.iteco.thirdtask.service.TransactionService;

import java.math.BigDecimal;
import java.util.List;

@RestController
@Validated
@Slf4j
public class AccountController {

    private final AccountService accountService;
    private final TransactionService transactionService;
    private final ObjectMapper objectMapper;

    public AccountController(AccountService accountService, TransactionService transactionService, ObjectMapper objectMapper) {
        this.accountService = accountService;
        this.transactionService = transactionService;
        this.objectMapper = objectMapper;
    }

    @GetMapping(value = "/info")
    public List<BankBookDto> getAllAccountByUser(@RequestHeader HttpHeaders headers) {

        log.info("headers: {}", headers);

        String userInfoStr = headers.getFirst("user-info");
        UserInfo userInfo;

        try {
            userInfo = objectMapper.readValue(userInfoStr, UserInfo.class);
        } catch (JsonProcessingException e) {
            throw new ValidateException("Данные пользователя заданы некорректно.");
        }

        return accountService.getBankBookByUserId(userInfo.getId(), headers);
    }

    @PostMapping(value = "/transact", produces = "application/json", consumes = "application/x-www-form-urlencoded")
    public Boolean transferBetweenBankBook(@RequestBody String transact,
                                           @RequestHeader HttpHeaders headers) {

        log.info("headers: {}", headers);
        List<String> data = List.of(transact.split("&"));

        String userInfoStr = headers.getFirst("user-info");
        UserInfo userInfo;

        try {
            userInfo = objectMapper.readValue(userInfoStr, UserInfo.class);
        } catch (JsonProcessingException e) {
            throw new ValidateException("Данные пользователя заданы некорректно.");
        }

        TransactionDto transactionDto;
        try {
            transactionDto =
                    TransactionDto.builder()
                            .sourceBankBookId(data.get(0).substring(7))
                            .targetBankBookId(data.get(1).substring(7))
                            .amount(BigDecimal.valueOf(Double.parseDouble(data.get(2).substring(7))))
                            .build();
        } catch (Exception ex) {
            throw new ParamException("Заданы неверные параметры: " + transact);
        }

        return transactionService.transferBetweenBankBook(transactionDto, userInfo);
    }
}
